package by.denprokazov.drunkvisor;

import android.support.annotation.NonNull;

public interface JsonParselableItem {
    @NonNull
    String getJsonArrayName();
    Class getClassInstance();
}
