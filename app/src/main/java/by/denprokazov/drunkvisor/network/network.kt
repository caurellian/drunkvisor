package by.denprokazov.drunkvisor.network

import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.fuel.httpPost
import com.github.kittinunf.result.Result
import com.github.kittinunf.result.getAs

const private val basicDomain: String = "http://192.168.43.99:5000"

fun getCategoryBlockingMode(): String  {
    val (request, response, result) = "$basicDomain/api/Categories".httpGet().responseString() // result is Result<String, FuelError>
    return result
}

fun getCatigories(): String {
    var data: String? = "failed"
    var error: String?
    "${basicDomain}/api/Categories".httpGet().responseString { request, response, result ->
        //do something with response
        when (result) {
            is Result.Failure -> {
                error = result.getAs()
            }
            is Result.Success -> {
                data = result.getAs()
            }
        }
    }
    return data?:"failed";
}

fun getDrinkDetails(id: Long): String {
    var data: String? = ""
    var error: String?
    "${basicDomain}/api/Drinks".httpGet().responseString { request, response, result ->
        //do something with response
        when (result) {
            is Result.Failure -> {
                error = result.getAs()
            }
            is Result.Success -> {
                data = result.getAs()
            }
        }
    }
    return data?:"";
}

fun getDrinkInfos(result: String): String {
    var data: String? = ""
    var error: String?
    "${basicDomain}/api/Drinks".httpPost().body(result).responseString { request, response, result ->
        //do something with response
        when (result) {
            is Result.Failure -> {
                error = result.getAs()
            }
            is Result.Success -> {
                data = result.getAs()
            }
        }
    }
    return data?:"";
}