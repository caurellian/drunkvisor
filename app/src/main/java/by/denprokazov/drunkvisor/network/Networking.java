package by.denprokazov.drunkvisor.network;

import android.os.StrictMode;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class Networking {


    final String url = "http://192.168.43.99:5000";
    OkHttpClient client = new OkHttpClient();
    public String getCategory() throws IOException {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);
        String url1 = url.concat("/api/Categories");
        Request request = new Request.Builder()
                .url(url1)
                .build();

        Response response = client.newCall(request).execute();
        return response.body().string();
    }
}
