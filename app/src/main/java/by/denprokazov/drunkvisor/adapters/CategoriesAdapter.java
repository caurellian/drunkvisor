package by.denprokazov.drunkvisor.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import by.denprokazov.drunkvisor.R;
import by.denprokazov.drunkvisor.structures.Category;
import by.denprokazov.drunkvisor.viewModels.CategoryViewModel;

public class CategoriesAdapter extends ArrayAdapter<String>{
    private final Context context;
    private final List<Category> categories;

    public CategoriesAdapter(Context context, List<Category> categories) {
        super(context, R.layout.category_list_item);
        this.context = context;
        this.categories = categories;
    }

    static class ViewHolder {
        ImageView categoryIV;
        TextView categoryNameTV;
    }

    @Override
    public int getCount() {
        return categories.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder  = new ViewHolder();
        View view = convertView;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.category_list_item, parent, false);
            viewHolder.categoryNameTV = (TextView) view.findViewById(R.id.category_name_textview);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }
        fillItems(position, viewHolder);
        return view;
    }

    private void fillItems(int position, ViewHolder viewHolder) {
        viewHolder.categoryNameTV.setText(categories.get(position).getName());
    }
}
