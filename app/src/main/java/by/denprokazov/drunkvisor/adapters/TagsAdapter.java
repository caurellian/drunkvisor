package by.denprokazov.drunkvisor.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import java.util.ArrayList;
import java.util.List;

import by.denprokazov.drunkvisor.R;
import by.denprokazov.drunkvisor.structures.Tag;

public class TagsAdapter extends ArrayAdapter<String> {

    private final Context context;
    private final List<Tag> tags;

    private List<Tag> checkedTags = new ArrayList<>();


    public TagsAdapter(Context context, List<Tag> tags) {
        super(context, R.layout.tag_list_item);
        this.context = context;
        this.tags = tags;
    }

    @Override
    public int getCount() {
        return tags.size();
    }

    public Tag getTagAtPosition(int position) {
        return tags.get(position);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = new ViewHolder();
        View view = convertView;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.tag_list_item, parent, false);
            viewHolder.tagCheckbox = (CheckBox) view.findViewById(R.id.tag_check_box);
            viewHolder.requestButton = (Button) view.findViewById(R.id.request_button);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }
        viewHolder.tagCheckbox.setText(tags.get(position).getName());
        viewHolder.tagCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    checkedTags.add(tags.get(position));
                } else {
                    checkedTags.remove(tags.get(position));
                }
            }
        });
        return view;
    }

    static class ViewHolder {
        Button requestButton;
        CheckBox tagCheckbox;
    }
    public List<Tag> getCheckedTags() {
        return checkedTags;
    }

}
