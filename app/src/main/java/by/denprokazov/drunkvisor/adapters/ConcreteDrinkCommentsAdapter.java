package by.denprokazov.drunkvisor.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import by.denprokazov.drunkvisor.R;
import by.denprokazov.drunkvisor.structures.Comment;

public class ConcreteDrinkCommentsAdapter extends ArrayAdapter<String>{
    private final List<Comment> comments;
    private final Context context;

    public ConcreteDrinkCommentsAdapter(Context context, List<Comment> comments) {
        super(context, R.layout.comment_item);
        this.comments = comments;
        this.context = context;
    }

    @Override
    public int getCount() {
        return comments.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder  = new ViewHolder();
        View view = convertView;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.concrete_drink_item, parent, false);
            viewHolder.commentTV = (TextView) view.findViewById(R.id.comment_item);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }
        viewHolder.commentTV.setText(comments.get(position).getComment());
        return super.getView(position, convertView, parent);
    }

    static class ViewHolder {
        TextView commentTV;
    }
}
