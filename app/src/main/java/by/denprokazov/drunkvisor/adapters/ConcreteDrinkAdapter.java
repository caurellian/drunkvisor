package by.denprokazov.drunkvisor.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

import by.denprokazov.drunkvisor.R;
import by.denprokazov.drunkvisor.structures.ConcreteDrinkModel;

public class ConcreteDrinkAdapter extends ArrayAdapter<String>{

    private final List<ConcreteDrinkModel> concreteDrinkModels;
    private final Context context;

    public ConcreteDrinkAdapter(Context context, List<ConcreteDrinkModel> concreteDrinkModels) {
        super(context, R.layout.concrete_drink_item);
        this.concreteDrinkModels = concreteDrinkModels;
        this.context = context;
    }

    @Override
    public int getCount() {
        return concreteDrinkModels.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder  = new ViewHolder();
        View view = convertView;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.concrete_drink_item, parent, false);
            viewHolder.concreteDrinkPicIV = (ImageView) view.findViewById(R.id.concrete_drink_image);
            viewHolder.concreteDrinkNameTV = (TextView) view.findViewById(R.id.concrete_drink_name_textview);
            viewHolder.concreteDrinkCommentsLV = (ListView) view.findViewById(R.id.comments_list_view);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        viewHolder.concreteDrinkNameTV.setText(concreteDrinkModels.get(position).getName());
        ConcreteDrinkCommentsAdapter concreteDrinkCommentsAdapter = new ConcreteDrinkCommentsAdapter(context,
                concreteDrinkModels.get(position).getComments());
        viewHolder.concreteDrinkCommentsLV.setAdapter(concreteDrinkCommentsAdapter);
        return super.getView(position, convertView, parent);
    }

    static class ViewHolder {
        ImageView concreteDrinkPicIV;
        TextView concreteDrinkNameTV;
        ListView concreteDrinkCommentsLV;
    }

}
