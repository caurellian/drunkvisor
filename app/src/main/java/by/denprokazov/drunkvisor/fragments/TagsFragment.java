package by.denprokazov.drunkvisor.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import by.denprokazov.drunkvisor.JSONHandler;
import by.denprokazov.drunkvisor.R;
import by.denprokazov.drunkvisor.TagsPost;
import by.denprokazov.drunkvisor.adapters.TagsAdapter;
import by.denprokazov.drunkvisor.structures.Tag;

public class TagsFragment extends Fragment {
    private JSONHandler<Tag> tagJSONHandler;
    private TagsAdapter tagsAdapter;
    private TagsPost tagsPost;
    private List<Tag> checkedTags;

    public void setTagsPost(TagsPost tagsPost) {
        this.tagsPost = tagsPost;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.list_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ListView tagsLV = (ListView) view.findViewById(R.id.main_list_view);
        Button requestButton = (Button) view.findViewById(R.id.request_button);

        try {
            Tag tag = new Tag();
            tagJSONHandler = new JSONHandler<>(tag);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            tagsAdapter = new TagsAdapter(this.getActivity(), tagJSONHandler
                    .parseJsonString(getString(R.string.json_mock_string)));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        tagsLV.setAdapter(tagsAdapter);
        requestButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkedTags = tagsAdapter.getCheckedTags();
                tagsPost.setTags(checkedTags);


                Toast.makeText(getActivity().getApplicationContext(),
                        getPostData(tagsPost), Toast.LENGTH_SHORT).show();
                if (checkedTags.size() == 0) {
                    Toast.makeText(getActivity().getApplicationContext(),
                            "U DUMB IDIOT", Toast.LENGTH_SHORT).show();
                } else {
                }
            }
        });
    }

    private String getPostData(TagsPost tagsPost){
        Gson gson = new Gson();
        String json = gson.toJson(tagsPost);
        return  json;
    }
}
