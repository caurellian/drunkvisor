package by.denprokazov.drunkvisor.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;
import org.json.JSONException;
import by.denprokazov.drunkvisor.JSONHandler;
import by.denprokazov.drunkvisor.R;
import by.denprokazov.drunkvisor.TagsPost;
import by.denprokazov.drunkvisor.adapters.CategoriesAdapter;
import by.denprokazov.drunkvisor.structures.Category;
import by.denprokazov.drunkvisor.network.NetworkKt;

public class CategoriesFragment extends Fragment {
    private JSONHandler<Category> categoryJSONHandler;
    private CategoriesAdapter categoriesAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.list_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ListView categoriesLV = (ListView) view.findViewById(R.id.main_list_view);
        try {
            Category category = new Category();
            categoryJSONHandler = new JSONHandler<>(category);
        } catch (JSONException e) {
            e.printStackTrace();
        }



        try {
            categoriesAdapter = new CategoriesAdapter(this.getActivity(),categoryJSONHandler
                    .parseJsonString(getString(R.string.json_mock_string)));
        } catch (JSONException e) {
            e.printStackTrace();
        }


        categoriesLV.setAdapter(categoriesAdapter);
        categoriesLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                android.app.FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();

                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);

                //Networking networking = new Networking();
                /*String sampleNetwork = null;
                sampleNetwork = NetworkKt.getCategoryBlockingMode();
                Toast.makeText(getActivity().getApplicationContext(), sampleNetwork, Toast.LENGTH_LONG).show();*/

                TagsPost tagsPost = new TagsPost();
                TagsFragment tagsFragment = new TagsFragment();
                tagsFragment.setTagsPost(tagsPost);
                tagsPost.setCategory_id(position);
                fragmentTransaction.replace(R.id.mainactivity_fragment_container, tagsFragment);
                fragmentTransaction.commit();
            }
        });
        }

}
