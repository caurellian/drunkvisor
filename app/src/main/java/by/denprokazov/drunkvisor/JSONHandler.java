package by.denprokazov.drunkvisor;

import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class JSONHandler<T extends JsonParselableItem> {
    private T tClass;


    public JSONHandler(T tClass) throws JSONException {
        this.tClass = tClass;
    }

    public ArrayList<T> parseJsonString(String jsonString) throws JSONException {
        String startString = "{" + tClass.getJsonArrayName() + ":" ;
        startString = startString.concat(jsonString);
        String endString  = "}";
        startString = startString.concat(endString);
        JSONObject jsonObject = toJsonObject(startString);
        JSONArray jsonArray = toJsonArray(jsonObject);
        return toObjectArray(jsonArray);
    }

    private JSONArray toJsonArray(JSONObject jsonObject) throws JSONException {
        return jsonObject.getJSONArray(tClass.getJsonArrayName());
    }

    private ArrayList<T> toObjectArray(JSONArray jsonArray) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        ArrayList<T> objectsArray = new ArrayList<>();
        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                tClass = (T) gson.fromJson(String.valueOf(jsonArray.getJSONObject(i)), tClass.getClassInstance());
                objectsArray.add(tClass);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return objectsArray;
    }

    @NonNull
    private JSONObject toJsonObject(String jsonString) throws JSONException {
        return (new JSONObject(jsonString));
    }
}

