package by.denprokazov.drunkvisor;

import java.util.ArrayList;
import java.util.List;

import by.denprokazov.drunkvisor.structures.Tag;

public class TagsPost {
    private int category_id;
    private List<Integer> tags;

    public void setCategory_id(int category_id) {
        this.category_id = category_id;
    }

    public void setTags(List<Tag> tagsArr) {
        List<Integer> tagsId = new ArrayList<>();
        for (Tag tag:
                tagsArr) {
            tagsId.add(tag.getId());
        }
        tags = tagsId;
    }
}
