package by.denprokazov.drunkvisor.viewModels;

public class CategoryViewModel {
    public int getId() {
        return Id;
    }

    public String getName() {
        return name;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    private int Id;
    private String name;
    private String imageUrl;
}
