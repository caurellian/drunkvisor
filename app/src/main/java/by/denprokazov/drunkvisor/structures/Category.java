package by.denprokazov.drunkvisor.structures;

import android.support.annotation.NonNull;

import java.util.List;

import by.denprokazov.drunkvisor.JsonParselableItem;

public class Category implements JsonParselableItem {
    private int Id;
    private String Name;

    public String getImage() {
        return Image;
    }

    private String Image;
    private List<Tag> Tags;

    public int getId() {
        return Id;
    }

    public String getName() {
        return Name;
    }

    @NonNull
    @Override
    public String getJsonArrayName() {
        return "Categories";
    }

    @Override
    public Class getClassInstance() {
        return Category.class;
    }
}
