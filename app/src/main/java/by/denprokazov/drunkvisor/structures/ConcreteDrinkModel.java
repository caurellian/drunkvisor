package by.denprokazov.drunkvisor.structures;

import java.util.List;

public class ConcreteDrinkModel {
    public int getId() {
        return Id;
    }

    public String getName() {
        return Name;
    }

    public String getImage() {
        return Image;
    }

    public List<Comment> getComments() {
        return Comments;
    }

    private int Id;
    private String Name;
    private String Image;
    private List<Comment> Comments;
}
