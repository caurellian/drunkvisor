package by.denprokazov.drunkvisor.structures;

public class Comment {
    public int getId() {
        return Id;
    }

    public String getDate() {
        return Date;
    }

    public String getComment() {
        return Comment;
    }

    private int Id;
    private String Date;
    private String Comment;
}
