package by.denprokazov.drunkvisor.structures;

import android.support.annotation.NonNull;

import by.denprokazov.drunkvisor.JsonParselableItem;

public class Tag implements JsonParselableItem {
    private int Id;
    private String Name;

    public int getId() {
        return Id;
    }

    public String getName() {
        return Name;
    }

    @NonNull
    @Override
    public String getJsonArrayName() {
        return "Tags";
    }

    @Override
    public Class getClassInstance() {
        return Tag.class;
    }
}
